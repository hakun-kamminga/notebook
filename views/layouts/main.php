<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

<link rel="stylesheet" href="/mvc/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/mvc/jquery-ui/jquery-ui.min.css">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
<style>
.gold{
color:#ffe901;
}
.green{
color:#0f3;
}
.align-right{
	text-align:right;
}
.delete{
	color:#f02;
}
.decrypt-cipher{
padding: 10px;
    width: 100%;
    font-family: monospace,'lucida console','courier new',fixedwidth;
    font-size: 13px;
    line-height: 13px;
    color: rgba(77, 77, 77, 0.9);
    text-shadow: 0 0 5px #fff;
    background: rgba(217, 217, 217, 0.8);
}
</style>
	</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => '<span class="fa fa-lock green"></span> Notebook',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'My Notes', 'url' => ['/notebook/my-notes']],
            ['label' => 'Send a note', 'url' => ['/notebook/add-note']],
            ['label' => 'Register', 'url' => ['/site/register']],
            empty(\Yii::$app->session['logged_in']['nu_email']) ? (
                ['label' => 'Login', 'url' => ['/notebook/my-notes']]
            ) : (
                ['label' => 'Logout', 'url' => ['/site/end-session']]
            ),/*
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            ) */
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Notebook <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>
<pre><?php // print_r($_SERVER); ?></pre>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
