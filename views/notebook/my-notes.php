<?php

/* @var $this yii\web\View */
$this->title = 'Add Note';
?>
<div class="site-index">
<h1>My Notes</h1>
<div class="body-content">
<form action="/" onsubmit="return login(this)">
<input type="hidden" name="action" value="decrypt_note">
<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>">
<div class="row">
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
 
 <div class="panel panel-primary">
 <div class="panel-heading">Your Notes</div>
 <div class="panel-body">
<?php

if (!empty($notes))
{
	//print_r($notes); 
	echo('<ul class="list-group">');
	foreach($notes as $note)
	{
		echo('
		<li class="list-group-item">
		 <div class="row">
		  <div class="col-lg-9 col-md-8 col-sm-6 col-xs-8">
		   <span class="fa fa-envelope"></span>  <a onclick="$(\'#pg_id\').val('.$note['pg_id'].')" href="#decrypt" role="button" class="pointer" data-toggle="modal"><strong>'.htmlentities($note['pg_title']).'</strong></a></span>
		  </div>
		  <div class="col-lg-3 col-md-4 col-sm-6 col-xs-4 align-right alignright rightalign right">
		   <a onclick="$(\'#pg_id\').val('.$note['pg_id'].')" href="#decrypt" role="button" class="pointer" data-toggle="modal">
		    <span class="fa fa-key green pointer"></span>
		   </a>
		   <span class="fa fa-trash delete pointer" onclick="if(confirm(\'Are you sure?\'))deleteNote('.$note['pg_id'].')"></span>
		  </div>
		 </div>
		</li>');
	}
	echo('</ul>');
}
else 
{
	echo("<p>No notes yet!</p>");
}

?>
</div> 
</div> 
</div>
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
 
 <div class="panel panel-success" id="private_key" style="display:none">
 <div class="panel-heading">Status</div>
 <div class="panel-body">
<pre id="prv"></pre>
</div> 
</div> 
</div>
</div> 
</form>
</div>
<!-- Modal -->

    <div class="modal fade" id="decrypt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-primary">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Decrypt Note</h4>
                </div>
                <div class="modal-body">
				<form action="/" onsubmit="return decryptNote(this)">
                    <textarea required name="private_key" id="private_key" class="decrypt-cipher" rows="20" cols="20" placeholder="Past your private key here"></textarea>
					<button class="btn btn-success"><span class="fa fa-key"></span> Decrypt note</button>
					<input type="hidden" name="action" value="decrypt" id="decrypt">
					<input type="hidden" name="pg_id" value="decrypt" id="pg_id">
				</form>
                </div>
            </div>
        </div>
    </div>