<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Notebook */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notebook-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nb_owner')->textInput() ?>

    <?= $form->field($model, 'nb_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nb_created_on')->textInput() ?>

    <?= $form->field($model, 'nb_locked')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
