<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NotebookSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notebook-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'nb_id') ?>

    <?= $form->field($model, 'nb_owner') ?>

    <?= $form->field($model, 'nb_title') ?>

    <?= $form->field($model, 'nb_created_on') ?>

    <?= $form->field($model, 'nb_locked') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
