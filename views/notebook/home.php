<h1>Notebook</h1>
<h2>Under construction</h2>
<div class="row">
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
 <button class="btn btn-success"><span class="fa fa-file-o"></span> Create a notebook</button>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
 <button class="btn btn-success"><span class="fa fa-key"></span> Create encryption keys</button>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
 <button class="btn btn-success"><span class="fa fa-plus"></span> Add a note</button>
</div>