<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
<h1>Register</h1>
<div class="body-content">
<form action="/" onsubmit="return register(this)">
<input type="hidden" name="action" value="register">
<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>">
<div class="row">
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
 
 <div class="panel panel-primary" id="register_form">
 <div class="panel-heading">Enter your email and a password to get started</div>
 <div class="panel-body">
 <div class="form-group">
  <label for="nb_title">Notebook Title</label>
  <div class="input-group">
   <input type="text" required class="form-control" name="nb_title" id="nb_title" placeholder="My Notes">
  </div>	
 </div>
 <div class="form-group">
  <label for="nu_handle">Handle (Will be published)</label>
  <div class="input-group">
   <input type="handle" required class="form-control" name="nu_handle" id="nu_handle">
  </div>	
 </div>
 <div class="form-group">
  <label for="email">Email</label>
  <div class="input-group">
   <input type="nu_email" class="form-control" required name="nu_email" id="nu_email">
  </div>	
 </div>
 <div class="form-group">
  <label for="nu_password">Password</label>
  <div class="input-group">
   <input type="password" class="form-control" name="nu_password" id="nu_password">
  </div>	
 </div>
 <div class="form-group">
  <button class="btn btn-default">Submit</button> &nbsp;<span id="save" style="display:none" class="fa fa-spinner fa-spin fa-fw"></span>
 </div>
</div> 
</div> 
</div>
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
 
 <div class="panel panel-primary" id="private_key" style="display:none">
 <div class="panel-heading">Generated Private Key. Store this safely!</div>
 <div class="panel-body">
<pre id="prv"></pre>
 <div class="form-group">
  <button class="btn btn-success">Copy</button>
 </div>
</div> 
</div> 
</div>
</div> 

<p>We do not store your private key in our database or on our filesystem. Keep it safe, and do not share it.</p>
<p>We won't share or publish your email but require it for account verification purposes.</p>
<p>Your password is stored as a secure hash in our database. Even if compromised (guessed), it can't be used to decrypt anything.</p>
</form>
</div>
