<?php

/* @var $this yii\web\View */
$this->title = 'Add Note';
?>
<div class="site-index">
<h1>Add Note</h1>
<div class="body-content">
<form action="/" onsubmit="return register(this)">
<input type="hidden" name="action" value="add_note">
<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>">
<div class="row">
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
 
 <div class="panel panel-primary">
 <div class="panel-heading">Select a recipient, or leave blank to send this note to yourself.</div>
 <div class="panel-body">
 <div class="form-group">
  <div class="form-group">
   <input type="text" required class="form-control" style="width:100%" name="nu_handle" id="nu_handle" placeholder="Recipient">
  </div>	 
  <div class="form-group">
   <input type="text" class="form-control" style="width:100%" name="pg_title" id="pg_title" placeholder="Note title (will NOT be encrypted)">
  </div>	 
  <div class="form-group">
   <textarea required class="form-control" rows="10" cols="40" style="width:100%" name="pg_content" id="pg_content" placeholder="Note"></textarea>
  </div>	
 </div>
 <div class="form-group">
  <button class="btn btn-success">Submit</button> &nbsp;<span id="save" style="display:none" class="fa fa-spinner fa-spin fa-fw"></span>
 </div>
</div> 
</div> 
<p>This note will be encrypted using the public key of the recipient.</p>
</div>
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
 
 <div class="panel panel-success" id="private_key" style="display:none">
 <div class="panel-heading">Status</div>
 <div class="panel-body">
<pre id="prv"></pre>
</div> 
</div> 
</div>
</div> 
</form>
</div>
  <script>
  $( function() {
    var availableHandles = [
<?php
if (!empty($handles))
{
	foreach($handles as $handle)
	{
		echo('"'.$handle['nu_handle'].'",'."\n");
	}
} ?>
    ];
    $( "#nu_handle" ).autocomplete({
      source: availableHandles
    });
  } );
  </script>