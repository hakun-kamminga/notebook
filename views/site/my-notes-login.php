<?php

/* @var $this yii\web\View */
$this->title = 'Add Note';
?>
<div class="site-index">
<h1>Login to see your notes</h1>
<div class="body-content">
<form action="/" onsubmit="return login(this)">
<input type="hidden" name="action" value="login">
<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>">
<div class="row">
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
 
 <div class="panel panel-primary">
 <div class="panel-heading">Please log in</div>
 <div class="panel-body">
 <div class="form-group"> 
  <div class="form-group">
   <input type="text" class="form-control" style="width:100%" name="nu_email" id="nu_email" placeholder="Email address">
  </div>	 
  <div class="form-group">
   <input type="password" class="form-control" style="width:100%" name="nu_password" id="nu_password" placeholder="password">
  </div>	
 </div>
 <div class="form-group">
  <button class="btn btn-success">Submit</button> &nbsp;<span id="save" style="display:none" class="fa fa-spinner fa-spin fa-fw"></span>
 </div>
</div> 
</div> 
</div>
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
  
 <div class="panel panel-success" id="private_key" style="display:none">
 <div class="panel-heading">Status</div>
 <div class="panel-body">
<pre id="prv"></pre>
</div> 
</div> 
</div>
</div> 
</form>
</div>