<?php

namespace app\controllers;
use PDO;
class NotebookController extends \yii\web\Controller
{
	public function actionGenerateKeys()
	{

		$data = 'plaintext data goes here';

		// Encrypt the data to $encrypted using the public key
		openssl_public_encrypt($data, $encrypted, $pubKey);

		// Decrypt the data using the private key and store the results in $decrypted
		openssl_private_decrypt($encrypted, $decrypted, $privKey);

		echo $decrypted;
	}
	public function actionMyNotes() 
	{		
		/**
		 * We use custom session tracking, not Yii2 RBAC or whatever.
		 * Keep it simple. We have our own custom login page (keeping the Yii2 one - but for other purposes).
		 *
		 * Since all notes are forever encrypted, the worst could be a breach of metadata anyway.
		 * Plus, we will enforce HTTPS somewhere down the line.
		*/		
		if (empty(\Yii::$app->session['logged_in']['nu_email']))
		{
			return $this->render("my-notes-login");
		}
		else
		{
			$notes = \Yii::$app->db->createCommand("SELECT * FROM notebook_page WHERE pg_owner = ".\Yii::$app->session['logged_in']['nu_id'])->queryAll();
			return $this->render("my-notes",[
				'notes' => $notes,
			]);
		}
	}
	public function actionAddNote()
	{
		$handles = \Yii::$app->db->createCommand("SELECT nu_id,nu_handle FROM notebook_user ORDER BY nu_handle ASC")->queryAll();
		
		return $this->render("add-note",[
			'handles' => $handles,
		]);
	}
	public function actionAjax()
	{
		switch($_POST['action'])
		{
			case "register":
			/**
			 * Already registered?
			*/
			$cmd = \Yii::$app->db->createCommand("SELECT nu_id FROM notebook_user WHERE nu_email = :email OR nu_handle = :handle LIMIT 1");
			$cmd->bindValue(":handle", $_POST['nu_handle'], PDO::PARAM_STR);
			$cmd->bindValue(":email",  $_POST['nu_email'],  PDO::PARAM_STR);
			if (!empty($cmd->queryOne()))
			{
				die("0");
			}
			/**
			 * Create a key pair based on the data sent by registrant.
			*/
			$hsh = \Yii::$app->getSecurity()->generatePasswordHash($_POST['nu_password']);
			$cfg = [
				"digest_alg"       => "sha512",
				"private_key_bits" => 4096,
				"private_key_type" => OPENSSL_KEYTYPE_RSA,
			];
			
			$res = openssl_pkey_new($cfg); 
			
			// openssl_pkey_export($res, $prv, !empty($_POST['password'])?$_POST['password']:'Yolanda'); // We don't want to enable pw protected pkeys this moment.
			
			openssl_pkey_export($res, $prv);
				   
			$pub = openssl_pkey_get_details($res);
			$pub = $pub["key"];			
				
			$sql = "INSERT INTO notebook_user (nu_handle, nu_password, nu_email, nu_pub, nu_created) VALUES (:handle, :passwd, :email, :pbkey, :today)";

			$cmd = \Yii::$app->db->createCommand($sql);
			$cmd->bindValue(":handle", $_POST['nu_handle'], PDO::PARAM_STR);
			$cmd->bindValue(":passwd", $hsh,  PDO::PARAM_STR);
			$cmd->bindValue(":email",  $_POST['nu_email'],  PDO::PARAM_STR);
			$cmd->bindValue(":pbkey",  $pub,                PDO::PARAM_STR);
			$cmd->bindValue(":today",  date('Y-m-d H:i:s'), PDO::PARAM_STR);

			if ($cmd->execute())
			{
				die($prv);
			}	
			else die('<span class="error">Could not create record.</span>');
			break;
			
			case "decrypt":
			if (!empty($_POST['pg_id']) && !empty($_POST['private_key']))
			{
				die(self::decryptMessage($_POST['pg_id'],$_POST['private_key']));
			}
			else 
			{
				if (empty($_POST['pg_id']))			die('No note specified!');
				if (empty($_POST['private_key']))	die('No key supplied!');
			}
			break;
			
			case "add_note":
			if (!empty($_POST['nu_handle'])) 
			{
				$sql = "SELECT * FROM notebook_user LEFT JOIN notebook ON notebook.nb_owner = notebook_user.nu_id WHERE nu_handle = :handle ";

				$cmd = \Yii::$app->db->createCommand($sql);
				$cmd->bindValue(":handle", $_POST['nu_handle'], PDO::PARAM_STR);	
				
				$data = $cmd->queryOne();
				
				if (!empty($data['nu_id']))
				{
					self::encryptMessage($_POST['pg_content'],$_POST['pg_title'],$data['nu_pub'],$data['nu_id']);
				}
				else
				{
					die('0');
				}				
			}
			break;
			
			case "login":
			$cmd = \Yii::$app->db->createCommand("SELECT * FROM notebook_user WHERE nu_email = :email LIMIT 1");
			$cmd->bindValue(":email",$_POST['nu_email'],PDO::PARAM_STR);
			
			$data = $cmd->queryOne();
			
			if (\Yii::$app->getSecurity()->validatePassword($_POST['nu_password'],$data['nu_password'])) 
			{
				unset($data['nu_password']);
				\Yii::$app->session['logged_in'] = $data;
				header("Content-Type: Application/JSON");
				die(json_encode(\Yii::$app->db->createCommand("SELECT * FROM notebook_page ")->queryAll()));
				
			} 
			else 
			{
				die("0");
			}			
			break;
			
			case "get_notes":
			die(json_encode(\Yii::$app->db->createCommand("SELECT * FROM notebook_page WHERE pg_owner = ".
				intval(\Yii::$app->session['logged_in']['nu_id']))->queryOne())
			);
			break;
		}
	}
	public static function encryptMessage($message,$title = '',$pub,$nu_id)
	{
		openssl_public_encrypt($message,$encrypted,$pub);
		/**
		 * Store encrypted message in DB
		*/
		$sql = "INSERT INTO notebook_page (pg_notebook_id,pg_title, pg_content, pg_created,pg_updated,pg_owner) VALUES (0,:title, :crypto, :today, :now, :owner)";

		$cmd = \Yii::$app->db->createCommand($sql);
		$cmd->bindValue(":owner",  $nu_id, 						PDO::PARAM_INT);
		$cmd->bindValue(":title",  $title, 						PDO::PARAM_STR);
		$cmd->bindValue(":crypto", base64_encode($encrypted), 	PDO::PARAM_STR);
		$cmd->bindValue(":today",  date('Y-m-d H:i:s'), 		PDO::PARAM_STR);
		$cmd->bindValue(":now",    date('Y-m-d H:i:s'), 		PDO::PARAM_STR);

		$cmd->execute();
		
		unset($message);
		
		die("$encrypted<br>$pub<br>$nu_id");
	}
	public static function decryptMessage($pg_id,$private_key)
	{
		$encrypted = \Yii::$app->db->createCommand("SELECT pg_content FROM notebook_page 
		
		WHERE  pg_id = ".intval($pg_id)." 
		AND pg_owner = ".intval(\Yii::$app->session['logged_in']['nu_id'])." LIMIT 1")->queryOne();
		
		if (!empty($encrypted) && !empty($private_key))
		{
			$pkey = openssl_pkey_get_private($private_key);
			
			openssl_private_decrypt(base64_decode($encrypted['pg_content']), $decrypted, $pkey);
			
			return $decrypted;
		}
		return false;
	}
	public function actionListHandles()
	{
	}
	public function actionIndex()
	{
		return $this->render("home");
	}
	public function actionAddAttachment()
	{
		return $this->render('add-attachment');
	}

	public function actionAddNotebook()
	{
		return $this->render('add-notebook');
	}

	public function actionDelete()
	{
		return $this->render('delete');
	}

	public function actionLock()
	{
		return $this->render('lock');
	}

	public function actionUnlock()
	{
		return $this->render('unlock');
	}
}
