<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Notebook;

/**
 * NotebookSearch represents the model behind the search form about `app\models\Notebook`.
 */
class NotebookSearch extends Notebook
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nb_id', 'nb_owner', 'nb_locked'], 'integer'],
            [['nb_title', 'nb_created_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Notebook::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'nb_id' => $this->nb_id,
            'nb_owner' => $this->nb_owner,
            'nb_created_on' => $this->nb_created_on,
            'nb_locked' => $this->nb_locked,
        ]);

        $query->andFilterWhere(['like', 'nb_title', $this->nb_title]);

        return $dataProvider;
    }
}
