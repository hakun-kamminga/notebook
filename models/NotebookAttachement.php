<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notebook_attachment".
 *
 * @property integer $at_id
 * @property integer $at_page
 * @property string $at_title
 * @property string $at_extension
 * @property string $at_created
 */
class NotebookAttachement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notebook_attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['at_page', 'at_created'], 'required'],
            [['at_page'], 'integer'],
            [['at_created'], 'safe'],
            [['at_title'], 'string', 'max' => 64],
            [['at_extension'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'at_id' => 'At ID',
            'at_page' => 'At Page',
            'at_title' => 'At Title',
            'at_extension' => 'At Extension',
            'at_created' => 'At Created',
        ];
    }
}
