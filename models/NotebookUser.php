<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notebook_user".
 *
 * @property integer $nu_id
 * @property string $nu_email
 * @property string $nu_pub
 * @property string $nu_prv
 * @property string $nu_password
 * @property string $nu_created
 * @property integer $nu_visible
 */
class NotebookUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notebook_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nu_email', 'nu_pub', 'nu_prv', 'nu_password', 'nu_created'], 'required'],
            [['nu_created'], 'safe'],
            [['nu_visible'], 'integer'],
            [['nu_email'], 'string', 'max' => 128],
            [['nu_pub', 'nu_prv', 'nu_password'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nu_id' => 'Nu ID',
            'nu_email' => 'Nu Email',
            'nu_pub' => 'Nu Pub',
            'nu_prv' => 'Nu Prv',
            'nu_password' => 'Nu Password',
            'nu_created' => 'Nu Created',
            'nu_visible' => 'Nu Visible',
        ];
    }
}
