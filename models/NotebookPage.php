<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notebook_page".
 *
 * @property integer $pg_id
 * @property integer $pg_notebook_id
 * @property string $pg_content
 * @property integer $pg_encrypted
 * @property string $pg_public_key
 * @property string $pg_created
 * @property integer $pg_creator
 * @property integer $pg_shared_with
 * @property string $pg_updated
 */
class NotebookPage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notebook_page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pg_notebook_id', 'pg_content', 'pg_public_key', 'pg_created', 'pg_creator', 'pg_updated'], 'required'],
            [['pg_notebook_id', 'pg_encrypted', 'pg_creator', 'pg_shared_with'], 'integer'],
            [['pg_content'], 'string'],
            [['pg_created', 'pg_updated'], 'safe'],
            [['pg_public_key'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pg_id' => 'Pg ID',
            'pg_notebook_id' => 'Pg Notebook ID',
            'pg_content' => 'Pg Content',
            'pg_encrypted' => 'Pg Encrypted',
            'pg_public_key' => 'Pg Public Key',
            'pg_created' => 'Pg Created',
            'pg_creator' => 'Pg Creator',
            'pg_shared_with' => 'Pg Shared With',
            'pg_updated' => 'Pg Updated',
        ];
    }
}
