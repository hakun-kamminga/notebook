<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notebook".
 *
 * @property integer $nb_id
 * @property integer $nb_owner
 * @property string $nb_title
 * @property string $nb_created_on
 * @property integer $nb_locked
 */
class Notebook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notebook';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nb_owner', 'nb_created_on'], 'required'],
            [['nb_owner', 'nb_locked'], 'integer'],
            [['nb_created_on'], 'safe'],
            [['nb_title'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nb_id' => 'Nb ID',
            'nb_owner' => 'Nb Owner',
            'nb_title' => 'Nb Title',
            'nb_created_on' => 'Nb Created On',
            'nb_locked' => 'Nb Locked',
        ];
    }
}
